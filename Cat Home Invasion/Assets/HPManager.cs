﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HPManager : MonoBehaviour {

    public Text bird_hp;
    public Text enemy_hp;
    public Text description_bird_attack;
    public Text description_enemy_attack;
    //public int current_bird_hp;

    

    // Use this for initialization
    void Start () {
        
		
	}
	
	// Update is called once per frame
	void Update () {

        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            Peck();

        }

        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Gust();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            FlyAttack();
        }


    }

    void Peck()
    {
        int ehp;
        int num = Random.Range(3, 10);
        ehp = int.Parse(enemy_hp.text);
        ehp -= num;
        enemy_hp.text = ehp.ToString();

        description_bird_attack.text = "you peck the cat for " + num.ToString() + " damage";

        FirstEnemyAttack();
        CheckDeath();




    }

    void Gust()
    {
        int ehp;
        int num = Random.Range(6, 8);
        ehp = int.Parse(enemy_hp.text);
        ehp -= num;
        enemy_hp.text = ehp.ToString();

        description_bird_attack.text = "you use gust and deal " + num.ToString() + " damage";

        FirstEnemyAttack();
        CheckDeath();


    }

    void FlyAttack()
    {

        int ehp;

        int num = Random.Range(0, 7);
        if(num==5)
        {
            num = 19;
        }
        else if( num== 6)
        {
            num = 20;
        }


        ehp = int.Parse(enemy_hp.text);
        ehp -= num;
        enemy_hp.text = ehp.ToString();

        description_bird_attack.text = "you use fly attack and deal " + num.ToString() + " damage";

        FirstEnemyAttack();
        CheckDeath();

    }


    void CheckDeath()
    {
        int bhp = int.Parse(bird_hp.text);
        int ehp = int.Parse(enemy_hp.text);
        if (bhp <= 0)
        {

            
            SceneManager.LoadScene("GameOver");

        }

        else if (ehp <= 0)
        {
            SceneManager.LoadScene("YouWin");
        }


    }


   

    void FirstEnemyAttack()
    {
        int bhp;
        int num = Random.Range(1, 15);
        bhp = int.Parse(bird_hp.text);
        bhp -= num;
        bird_hp.text = bhp.ToString();

        description_enemy_attack.text= "cat attacks you for " + num.ToString() + " damage";


    }
}
